#!/bin/ash
echo 'ipod:x:1000:1000:Linux User,,,:/home/ipod:/bin/bash' >> /etc/passwd
echo 'ipod:x:1000:' >> /etc/group
echo 'ipod:$6$eR7CorR/EKt7stBW$kBoSKys3BUVb0vZMqSGI.lfDsAk.RRDvIXV4UIp3i4b7FuVkFpfiTThO1Epo0QdtbFxifynyJixnQaLwA.S33/:17087:0:99999:7:::' >> /etc/shadow
mkdir /home/ipod
chown -R ipod:ipod /home/ipod
addgroup ipod docker
echo 'ipod   ALL=(ALL) ALL' >> /etc/sudoers
echo 'root:$6$eR7CorR/EKt7stBW$kBoSKys3BUVb0vZMqSGI.lfDsAk.RRDvIXV4UIp3i4b7FuVkFpfiTThO1Epo0QdtbFxifynyJixnQaLwA.S33/'  | chpasswd -e
sed '$d' -i /etc/ssh/sshd_config
