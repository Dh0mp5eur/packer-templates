#!/bin/bash

# download jq parser
if [[ ! -f /usr/bin/jq ]]; then
    /usr/bin/wget http://stedolan.github.io/jq/download/linux64/jq -P /usr/bin
    /bin/chmod u+x /usr/bin/jq
fi

usage() {
  echo -e "Pass required options to vm_create \n-n {nodename}\n-d {disk size - in GBs, ONLY numeric value}\n-h {hostname}\n-b {vmbr[0-9]}\n-m {memory - in MBs, ONLY numeric value)\n-s {sockets}\n-c {cores}"
  exit 1
}

if (($# == 0)); then
  usage
fi

while getopts ":n:d:p:b:h:m:s:c:" opt; do
  case $opt in
    n)
      NODE=$OPTARG
      #echo "-n was triggered, Parameter: $OPTARG" >&2
      ;;
    d)
      SIZE=$OPTARG
      #echo "-s was triggered, Parameter: $OPTARG" >&2
      ;;
    p)
      STORAGE=$OPTARG
      #echo "-s was triggered, Parameter: $OPTARG" >&2
      ;;
    b)
      VMBR=$OPTARG
      #echo "-b was triggered, Parameter: $OPTARG" >&2
      ;;
    h)
      HOSTNAME=$OPTARG
      #echo "-s was triggered, Parameter: $OPTARG" >&2
      ;;
    m)
      MEMORY=$OPTARG
      #echo "-s was triggered, Parameter: $OPTARG" >&2
      ;;
    s)
      SOCKETS=$OPTARG
      #echo "-s was triggered, Parameter: $OPTARG" >&2
      ;;
    c)
      CORES=$OPTARG
      #echo "-s was triggered, Parameter: $OPTARG" >&2
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# get next available vmid
  echo "Getting next available vm id..."
  VMID=`/usr/bin/pvesh get /cluster/nextid | tr -d '"'`
  echo "VMID is $VMID"

# get storage path
  echo "Getting stotage path of $STORAGE"
  STORAGE_PATH=`pvesh get storage/$STORAGE | jq '.path | tostring' | tr -d '"'`
  echo "Storage Path is $STORAGE_PATH"

# copy disk
  if [[ ! -d $STORAGE_PATH/images/$VMID ]]; then
    mkdir $STORAGE_PATH/images/$VMID
  fi
  cp output-qemu/$HOSTNAME $STORAGE_PATH/images/$VMID/vm-$VMID-disk-0.qcow2

# create vm
  echo "Creating VM..."
  pvesh create /nodes/$NODE/qemu \
  -name "$HOSTNAME-$VMID" \
  -vmid $VMID \
  -memory $MEMORY \
  -sockets $SOCKETS \
  -cores $CORES \
  -net0 virtio,bridge=$VMBR \
  -virtio0 $STORAGE:$VMID/vm-$VMID-disk-0.qcow2

# Start after create
  echo "Starting VM$VMID"
  pvesh create /nodes/$NODE/qemu/$VMID/status/start
